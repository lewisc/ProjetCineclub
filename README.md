---
lang: fr
---

# ![](ressources/logo.jpg) R.4.01 Développement Web - JavaScript

**IUT Montpellier-Sète – Département Informatique**

# Projet Parcours B & D - Cinéclub

## CONCLUSION SUR LE PROJET

**URL où le projet est déployé :**
https://webinfo.iutmontp.univ-montp2.fr/~lewisc/WEB/ProjetCineclub/src/index.html

**Répartition du travail en pourcentages :**

- Charlotte Lewis : 38% (Refactoriser le code pour implémenter le reactive + création de la bibliothèque reactive + implémenter la logique pour ajouter un film ou un membre + correction du problème des notes qui ne s'affichait pas correctement)
- Jade Renaut : 38% (Suppression de membres et de films + css + l'autocomplétion lorsque l'on ajoute un film + correction de bugs + mettre une note (version initiale sans librairie reactive))
- Paul Jaquemin : 24% (Popups + suppression de membres et de films + travail sur les notes)

**Remarques divers :**
- Nous avons rencontré une première difficulté au niveau du choix des notes pour un film. En effet, l'interface était mise à jour avant que la note correspondante soit calculée, ce qui faisait qu'aucune note n'était prise en compte. Pour régler ce problème, nous avons utilisé de multiples fonctions asynchrones en s'assurant d'attendre la réponse de ces dernières avant de poursuivre dans la logique du code.
- Une deuxième difficulté a été rencontrée lorsque nous avons essayé de refactoriser notre code en enlevant les getters pour les remplacer pour une logique reactive. Nous avons tenté tout d'abord d'effectuer le refactoring avec la bibliothèque fournie dans le TD7 mais nous nous sommes très vite rendus comptes que certains éléments avaient besoin d'une logique qui n'existait pas dans cette bibliothèque. Nous avons donc ré-écrit la bibliothèque en ajoutant des éléments comme data-input (pour détecter lorsqu'on écrit dans un input).
- Au niveau des inputs, il fallait qu'ils aient leur variable reactive mis à jour lorsque l'on écrivait dans un input, mais il était aussi nécessaire de changer l'input si du côté du variable réactive le contenu changeait. Nous avons donc mis en place une sorte de "double-binding" en utilisant à la fois un data-input et un data-textvar.
- Il se peut que le CSS ajouté ne fonctionne pas sur **Chrome**
