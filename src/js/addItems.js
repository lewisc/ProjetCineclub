import { reactive } from "./reactiveLib/reactive.js";
import { filmsTable } from "./reactiveFilms.js";
import { membersTable } from "./reactiveMembers.js";

let inputNvMembre = reactive({
    content: ''
}, "inputNvMembre");

inputNvMembre.updateContent = function(content) {
    inputNvMembre.content = content;
}

inputNvMembre.addNewMember = function () {
    if (!isEmptyOrSpaces(inputNvMembre.content)) {
        const memberName = inputNvMembre.content.trim();

        // Vérifier si le nom du membre n'existe pas déjà dans la liste
        if (!membersTable.members.includes(memberName)) {
            const data = new FormData();
            data.set('action', 'add');
            data.set('nom', memberName);

            fetch('php/set.php', {
                method: 'POST',
                body: data
            })
                .then(() => {
                    // Mettre à jour la table des membres
                    membersTable.updateMembersHTML('');
                    inputNvMembre.content = '';
                });
        } else {
            warningPopup("Ce membre existe déjà.");
        }
    } else {
        warningPopup("Le nom du nouveau membre ne doit pas être vide.");
    }
}

let inputNvFilm = reactive({
    content: ''
}, "inputNvFilm");

inputNvFilm.updateContent = function (content) {
    inputNvFilm.content = content;
}

inputNvFilm.addNewFilm = function() {
    if (!isEmptyOrSpaces(inputNvFilm.content)) {
        const filmTitle = inputNvFilm.content.trim();

        // Vérifier si le titre du film n'existe pas déjà dans la liste
        if (!filmsTable.films.includes(filmTitle)) {
            const data = new FormData();
            data.set('action', 'add');
            data.set('titre', filmTitle);

            fetch('php/set.php', {
                method: 'POST',
                body: data
            })
                .then(() => {
                    inputNvFilm.content = '';
                    filmsTable.updateFilms();
                })
                .catch(error => {
                    console.error('Request failed:', error);
                });
        } else {
            warningPopup("Ce film existe déjà.");
        }
    } else {
        warningPopup("Le titre du nouveau film ne doit pas être vide.");
    }
}

function isEmptyOrSpaces(str) {
    return str.trim() === '';
}

const inputField = document.getElementById('titre-nouveau-film');
const autocompleteList = document.getElementById('autocompletion');

inputField.addEventListener('input', function() {
    const inputValue = this.value.trim();

    if (inputValue === '') {
        autocompleteList.innerHTML = '';
        return;
    }

    fetch(`https://webinfo.iutmontp.univ-montp2.fr/~etupoupet/movies.php?s=${inputValue}`)
        .then(response => response.json())
        .then(data => {
            autocompleteList.innerHTML = '';

            for (let i = 0; i < Math.min(5, data.length); i++) {
                const movie = data[i];
                const option = document.createElement('div');
                option.textContent = movie;
                option.addEventListener('click', function() {
                    inputNvFilm.updateContent(movie);
                    autocompleteList.innerHTML = '';
                });
                autocompleteList.appendChild(option);
            }
        })
        .catch(error => {
            console.error('Erreur lors de la récupération des données :', error);
        });
});

export { inputNvFilm, inputNvMembre };
