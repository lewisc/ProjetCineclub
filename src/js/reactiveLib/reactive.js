let objectByName = new Map();
let registeringEffect = null;
let objetDependencies = new Map();

function applyAndRegister(effect) {
    registeringEffect = effect;
    effect();
    registeringEffect = null;
}

function trigger(target, key) {
    if (!objetDependencies.has(target) || !objetDependencies.get(target).has(key)) {
        return;
    }
    for (let effect of objetDependencies.get(target).get(key)) {
        effect();
    }
}

function reactive(passiveObject, name) {
    objetDependencies.set(passiveObject, new Map());
    const handler = {
        get(target, key) {
            if (registeringEffect !== null)
                registerEffect(registeringEffect, target, key);
            return target[key];
        },
        set(target, key, value) {
            target[key] = value;
            trigger(target, key);
            return true;
        },
    };

    const reactiveObject = new Proxy(passiveObject, handler);
    objectByName.set(name, reactiveObject);
    return reactiveObject;
}

function registerEffect(effect, target, key) {
    if (!objetDependencies.has(target)) {
        objetDependencies.set(target, new Map());
    }
    if (!objetDependencies.get(target).has(key)) {
        objetDependencies.get(target).set(key, new Set());
    }
    objetDependencies.get(target).get(key).add(effect);
}

function startReactiveDom(subDOM = document) {
    subDOM.addEventListener('click', (event) => {
        const target = event.target;
        if (target.dataset.onclick) {
            const [obj, method, ...arg] = target.dataset.onclick.split(/[.,()]+/);
            objectByName.get(obj)[method](...arg);
        }
    });
    subDOM.addEventListener('input', (event) => {
        const target = event.target;
        if (target.dataset.input) {
            const [obj, method] = target.dataset.input.split('.');
            objectByName.get(obj)[method](target.value);
        }
    });

    for (let element of subDOM.querySelectorAll("[data-htmlvar], [data-textvar]")) {
        if (element.dataset.htmlvar) {
            const [obj, prop] = element.dataset.htmlvar.split('.');
            applyAndRegister(() => { element.innerHTML = objectByName.get(obj)[prop] });
        }
        if (element.dataset.textvar) {
            const [obj, prop] = element.dataset.textvar.split('.');
            applyAndRegister(() => {
                element.value = objectByName.get(obj)[prop];
            });
        }
    }
}

export { reactive, applyAndRegister, startReactiveDom }
