afficherNotesMembres = async function (membre) {
  const data = await fetch(`php/get.php?q=notes&membre=${membre}`);
  let notes = await data.json();
  let output = "Notes attribués par ce membre :\n";
  for (const note of notes) {
    output += `${note.titre}: ${note.valeur}\n`;
  }
  window.alert(output);
};

afficherNotesFilm = async function (flilm) {
  const data = await fetch(`php/get.php?q=notes&film=${flilm}`);
  let notes = await data.json();
  let output = "Notes attribués à ce film :\n";
  for (const note of notes) {
    output += `${note.nom}: ${note.valeur}\n`;
  }
  window.alert(output);
};

function warningPopup(message) {
  window.alert(message);
}
