import { reactive } from "./reactiveLib/reactive.js";
import {membersTable, optionsMembers} from "./reactiveMembers.js";

let filmsTable = reactive({
    content: '',
    films: []
}, "filmsTable");

filmsTable.updateFilms = async function () {
    try {
        const response = await fetch("php/get.php?q=all");
        const data = await response.json();

        filmsTable.content = '';
        const selectedMemberID = optionsMembers.selectedOptionID;
        const notesByFilm = {};

        // La somme des notes et le nombre de notes pour chaque film
        data.notes.forEach((note) => {
            if (!notesByFilm[note.id_film]) {
                notesByFilm[note.id_film] = { sum: 0, count: 0 };
            }
            notesByFilm[note.id_film].sum += note.valeur;
            notesByFilm[note.id_film].count++;
        });

        for (const film of data.films) {
            // Ajouter le membre au tableau members
            filmsTable.films.push(film.titre);

            let rowHTML = `<tr>`;

            const averageNote = notesByFilm[film.id]
                ? (notesByFilm[film.id].sum / notesByFilm[film.id].count).toFixed(1)
                : "";

            const filmNameHTML = `<td onclick="afficherNotesFilm(${film.id})">${film.titre}</td>`;
            const averageNoteHTML = `<td>${averageNote}</td>`;

            rowHTML += `${filmNameHTML}${averageNoteHTML}`;

            // Si un membre est sélectionné, chercher sa note pour chaque film
            if (selectedMemberID.toString() !== '') {
                const memberNoteValueForFilm = await getNote(selectedMemberID, film.id); // Wait for the note

                let noteHTML = `<td>`;
                let selectHTML = `<select id="note-${film.id}">`;

                const noteOptions = ["-", "0", "1", "2", "3", "4", "5"];
                noteOptions.forEach((optionValue) => {
                    selectHTML += `<option value="${optionValue}" data-onclick="filmsTable.handleSelectChange(${selectedMemberID}, ${film.id}, ${optionValue})" `;
                    if (memberNoteValueForFilm !== null && memberNoteValueForFilm !== undefined && memberNoteValueForFilm.toString() === optionValue.toString()) selectHTML += " selected";
                    selectHTML += `>${optionValue}</option>`;
                });

                selectHTML += `</select>`;
                noteHTML += `${selectHTML}</td>`;
                rowHTML += `${noteHTML}`;
            }


            // Supprimer un film
            let deleteBtnHTML = `<td><button data-onclick="filmsTable.deleteFilm(${film.id})">Supprimer</td></button>`;
            rowHTML += `${deleteBtnHTML}</tr>`;


            filmsTable.content += rowHTML;
        }
    } catch (error) {
        console.error("Erreur lors de la récupération des films :", error);
    }
}


async function getNote(memberID, filmID) {
    try {
        const response = await fetch(`php/get.php?q=notes&membre=${memberID}`);
        const results = await response.json();

        let note = "-";

        for (let i = 0; i < results.length; i++) {
            const result = results[i];
            if (result.id.toString() === filmID.toString()) {
                note = result.valeur;
                return note;
            }
        }

        return note;
    } catch (error) {
        console.error("Error fetching note:", error);
    }

}

filmsTable.deleteFilm = function(filmID) {
    if (
        confirm(
            `Êtes-vous sûr de vouloir supprimer ce film ?`,
        )
    ) {
        fetch(`php/set.php`, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: `action=delete&film=${filmID}`,
        }).then((response) => {
            if (response.ok) {
                optionsMembers.updateSelectedOption(optionsMembers.selectedOptionID);
                membersTable.updateMembersHTML(optionsMembers.selectedOptionID);
            } else {
                console.error("Erreur lors de la suppression du film");
            }
        });
    }
}

filmsTable.handleSelectChange = function (memberID, filmID, value) {
    filmsTable.updateNote(memberID, filmID, value);
}

filmsTable.updateNote = function(memberId, filmId, noteValue) {
    let noteToSend = noteValue;

    // If noteValue is "-", set it to -1 for the server
    if (noteValue.trim() === "-") {
        noteToSend = -1;
    }


    const requestBody = `action=update&membre=${memberId}&film=${filmId}&note=${noteToSend}`;

    fetch(`php/set.php`, {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: requestBody,
    }).then((response) => {

        if (response.ok) {
            membersTable.updateMembersHTML(memberId).then(()=>{
                optionsMembers.updateSelectedOption(memberId);
            })
        } else {
            console.error("Server error:", response.status, response.statusText);
            // Log the response body if available
            response.text().then((text) => {
                console.error("Response body:", text);
            });
        }
    }).catch((error) => {
        console.error("Fetch error:", error);
    });
}


export {filmsTable}
