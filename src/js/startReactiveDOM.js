import {applyAndRegister, startReactiveDom} from "./reactiveLib/reactive.js";
import {membersTable} from "./reactiveMembers.js";
import {filmsTable} from "./reactiveFilms.js";

applyAndRegister(()=>filmsTable.updateFilms());
applyAndRegister(() => membersTable.updateMembersHTML(''));
startReactiveDom();