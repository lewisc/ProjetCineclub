import { reactive } from "./reactiveLib/reactive.js";
import { filmsTable } from "./reactiveFilms.js";

let membersTable = reactive({
    membersHTML: '',
    members: []
}, "membersTable");

let optionsMembers = reactive({
    optionsHTML: '',
    selectedOptionID: ''
}, "optionsMembers");

optionsMembers.updateSelectedOption = function(selectedOptionID) {
    optionsMembers.selectedOptionID = selectedOptionID;
    filmsTable.updateFilms();
}


membersTable.updateMembersHTML = function(selectedMemberID) {
    return fetch("php/get.php?q=all")
        .then((response) => response.json())
        .then((data) => {
            optionsMembers.selectedOptionID = selectedMemberID;

            const notesCountMap = {};
            const membres = data.membres;
            const notes = data.notes;

            membres.forEach((member) => {
                notesCountMap[member.id] = getNbNotes(member, notes);
            });

            let tableRows = '';
            let options = `<option data-onclick="optionsMembers.updateSelectedOption()"></option>`;
            membres.forEach((member) => {
                // Ajouter le membre au tableau members
                membersTable.members.push(member.nom);

                tableRows += getMemberInfoHTML(member, notesCountMap);
                options += `<option value="${member.id}" data-onclick="optionsMembers.updateSelectedOption(${member.id}, ${member.nom})"`;
                if (selectedMemberID.toString() === member.id.toString()) options+=` selected`;
                options += `>${member.nom}</option>`
            });

            membersTable.membersHTML = tableRows;
            optionsMembers.optionsHTML = options;

        })
        .catch((error) => {
            console.error("Erreur lors de la récupération des membres :", error);
            membersTable.membersHTML = '';
        });
}

membersTable.deleteMemberHTML = (memberID) => {
    //console.log("Delete member asked");
    if (
        confirm(
            `Êtes-vous sûr de vouloir supprimer ce membre ?`,
        )
    ) {
        //console.log("Delete member confirmed");
        fetch(`php/set.php`, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: `action=delete&membre=${memberID}`,
        }).then((response) => {
            if (response.ok) {
                membersTable.updateMembersHTML('');
                filmsTable.updateFilms();
            } else {
                console.error("Erreur lors de la suppression du membre");
                throw new Error("Network response was not ok.");
            }
        }).catch((error) => {
            console.error("Error:", error);
        });
    }
}


function getNbNotes(member, notes) {
    const id = member.id;
    const memberNotes = notes.filter((note) => note.id_membre === id && note.valeur !== "-");
    return memberNotes.length;
}

function getMemberInfoHTML(member, notesCountMap) {
    return `<tr>
                <td onclick="afficherNotesMembres(${member.id})">${member.nom}</td>
                <td>${notesCountMap[member.id] || 0}</td>
                <td>
                    <button data-onclick="membersTable.deleteMemberHTML('${member.id}')">Supprimer</button>
                </td>
            </tr>`;
}

export { membersTable, optionsMembers };
